
#include <iostream>
#include <type_traits>

// sample(unbound_increment)
template<class T>
void increment(T& x)
{
    ++x;
}

template<class T>
void twice(T& x)
{
    increment(x);
    increment(x);
}
// end-sample

struct foo {};

int main() {
    int i = 1;
    twice(i);
    std::cout << i; // Prints 3

    // foo f;
    // twice(f);
}
