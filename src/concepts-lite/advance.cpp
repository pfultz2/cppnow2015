#include <iostream>
#include <vector>
#include <list>
#include <type_traits>

template<class T>
concept bool Incrementable()
{
    return requires(T x)
    {
        { ++x } -> T&;
        { x++ } -> T;
    };
}

template<class T>
concept bool Decrementable()
{
    return Incrementable<T>() && requires(T x)
    {
        { --x } -> T&;
        { x-- } -> T;
    };
}

template<class T, class Number>
concept bool Advanceable()
{
    return Decrementable<T>() && requires(T x, Number n)
    {
        { x += n } -> T&;
    };
}

template<class Iterator> requires Advanceable<T, int>()
void advance(Iterator& it)
{
    it += n;
}

template<class Iterator> requires Decrementable<T>()
void advance(Iterator& it, int n)
{
    if (n > 0) while (n--) ++it;
    else 
    {
        n *= -1;
        while (n--) --it;
    }
}

template<class Iterator> requires Incrementable<T>()
void advance(Iterator& it, int n)
{
    while (n--) ++it;
}

void check_list()
{
    std::list<int> l = { 1, 2, 3, 4, 5, 6 };
    auto iterator = l.begin();
    advance(iterator, 4);
    std::cout << *iterator << std::endl;
}

void check_reverse_list()
{
    std::list<int> l = { 1, 2, 3, 4, 5, 6 };
    auto iterator = l.end();
    advance(iterator, -4);
    std::cout << *iterator << std::endl;
}

void check_vector()
{
    std::vector<int> v = { 1, 2, 3, 4, 5, 6 };
    auto iterator = v.begin();
    advance(iterator, 4);
    std::cout << *iterator << std::endl;
}

struct foo {};

int main()
{
    check_list();
    check_reverse_list();
    check_vector();
}
