#include <iostream>
#include <vector>
#include <list>
#include <type_traits>
#include <boost/variant.hpp>
#include <boost/fusion/container/generation/make_vector.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/adapted/std_tuple.hpp>
#include <boost/fusion/support/is_sequence.hpp>

namespace adl {

using std::begin;
using std::end;

template<class R>
auto adl_begin(R&& r) -> decltype(begin(r));

template<class R>
auto adl_end(R&& r) -> decltype(end(r));
}

template<class Stream, class T>
concept bool Streamable()
{
    return requires(Stream&& s, T&& x)
    {
        s << x;
    };
}

template<class T>
concept bool Iterator()
{
    return std::is_copy_constructible<T>() && 
        std::is_copy_assignable<T>() &&
        std::is_destructible<T>() &&
        requires(T x)
        {
            *x;
            { ++x } -> T&;
        };
}

template<class T>
concept bool Range()
{
    return requires(T&& x)
    {
        { adl::adl_begin(x) };// -> Iterator;
        { adl::adl_end(x) };// -> Iterator;
    };
}

void print(const std::string& x)
{
    std::cout << x << std::endl;
}

template<class R> requires Range<R>()
void print(const R& r);

template<class Sequence> requires boost::fusion::traits::is_sequence<Sequence>() and not Range<Sequence>()
void print(const Sequence& s);

template<class... Ts>
void print(const boost::variant<Ts...>& v);

template<class T> requires Streamable<std::ostream, T>() and not boost::fusion::traits::is_sequence<T>() and not Range<T>()
void print(const T& x);


template<class R> requires Range<R>()
void print(const R& r)
{
    for(const auto& x:r) print(x);
}

template<class Sequence> requires boost::fusion::traits::is_sequence<Sequence>() and not Range<Sequence>()
void print(const Sequence& s)
{
    boost::fusion::for_each(s, [](const auto& x)
    {
        print(x);
    });
}

struct variant_print
{
    typedef void result_type;
    template<class T>
    auto operator()(const T& x) const
    {
        print(x);
    }
};

template<class... Ts>
void print(const boost::variant<Ts...>& v)
{
    boost::apply_visitor(variant_print(), v);
}

template<class T> requires Streamable<std::ostream, T>() and not boost::fusion::traits::is_sequence<T>() and not Range<T>()
void print(const T& x)
{
    std::cout << x << std::endl;
}

int main()
{
    static_assert(Iterator<int*>(), "Failed iterator");
    static_assert(Range<const std::vector<int>>(), "Failed Range");
    static_assert(Range<int[5]>(), "Failed Range");
    print("hello");
    print("3");
    
    int i = 3;
    print(i);

    std::vector<int> r = { 3, 3, 3 };
    print(r);

    auto t = std::make_tuple(3, 3, 3);
    print(t);

    auto f = boost::fusion::make_vector(3, 3, 3);
    print(f);

    boost::variant<int, std::string> v1 = 3;
    print(v1);
    boost::variant<int, std::string> v2 = "3";
    print(v2);

    std::vector<std::string> rs = { "hello", "world"};
    print(rs);

    boost::variant<int, std::vector<int>> v3 = r;
    print(v3);

    std::vector<boost::variant<int, std::string>> rv = { 3, "3", 3 };
    print(rv);

    auto m = std::make_tuple(3, r, v1, v3, rv);
    print(m);
}
