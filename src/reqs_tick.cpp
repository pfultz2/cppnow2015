

#include <iostream>
#include <type_traits>
#include <tick/builder.h>
#include <tick/builder.h>
#include <tick/requires.h>

// sample(tick_incrementable)
TICK_TRAIT(is_incrementable)
{
    template<class T>
    auto require(T&& x) -> valid<
        decltype(x++),
        decltype(++x)
    >;
};
// end-sample

// sample(tick_increment)
template<class T>
void increment(T& x)
{
    ++x;
}

template<class T, TICK_REQUIRES(is_incrementable<T>())>
void twice(T& x)
{
    increment(x);
    increment(x);
}
// end-sample

struct foo {};

int main() {
    int i = 1;
    twice(i);
    std::cout << i; // Prints 3

    // foo f;
    // twice(f);
}
