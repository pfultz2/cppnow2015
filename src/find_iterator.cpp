#include <vector>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <algorithm>
#include <fit/function.h>
#include <fit/infix.h>
#include <fit/conditional.h>
#include <fit/compose.h>
#include <fit/placeholders.h>

// sample(fit_find_iterator)
FIT_STATIC_FUNCTION(find_iterator) = fit::conditional(
    [](const auto& r, const auto& x) -> decltype(find(r, x))
    {
        return find(r, x);
    },
    [](const std::string& s, const auto& x)
    {
        auto index = s.find(x);
        if (index == std::string::npos) return s.end();
        else return s.begin() + index;
    },
    [](const auto& r, const auto& x) -> decltype(r.find(x))
    {
        return r.find(x);
    },
    [](const auto& r, const auto& x)
    {
        using std::begin;
        using std::end;
        return std::find(begin(r), end(r), x);
    }
);
// end-sample

FIT_STATIC_FUNCTION(in) = fit::infix(
    [](const auto& x, const auto& r)
    {
        using std::end;
        return find_iterator(r, x) != end(r);
    }
);

constexpr auto not_in = fit::infix(fit::compose(not fit::_, in));

int main()
{
    std::vector<int> numbers = { 1, 2, 3, 4, 5 };
    if (5 <in> numbers) std::cout << "Yes" << std::endl;

    std::string s = "hello world";
    if ("hello" <in> s) std::cout << "Yes" << std::endl;

    std::map<int, std::string> number_map = {
        { 1, "1" },
        { 2, "2" },
        { 3, "3" },
        { 4, "4" }
    };

    if (4 <in> number_map) std::cout << "Yes" << std::endl;

    if (not (8 <in> numbers)) std::cout << "No" << std::endl;
    if (8 <not_in> numbers) std::cout << "No" << std::endl;

}
