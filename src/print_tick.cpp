#include <iostream>
#include <vector>
#include <list>
#include <boost/variant.hpp>
#include <boost/fusion/container/generation/make_vector.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/adapted/std_tuple.hpp>
#include <boost/fusion/support/is_sequence.hpp>
#include <tick/builder.h>
#include <tick/requires.h>
#include <fit/fix.h>
#include <fit/result.h>
#include <fit/lambda.h>
#include <fit/conditional.h>


namespace adl {

using std::begin;
using std::end;

template<class R>
auto adl_begin(R&& r) -> decltype(begin(r));

template<class R>
auto adl_end(R&& r) -> decltype(end(r));
}

// sample(print_tick_traits)
TICK_TRAIT(is_streamable)
{
    template<class Stream, class T>
    auto require(Stream&& s, T&& x) -> valid<
        decltype(s << x)
    >;
};

TICK_TRAIT(is_iterator, 
    std::is_copy_constructible<_>, 
    std::is_copy_assignable<_>, 
    std::is_destructible<_>)
{
    template<class T>
    auto require(T x) -> valid<
        decltype(*x),
        decltype(returns<T&>(++x))
    >;
};

TICK_TRAIT(is_range)
{
    template<class T>
    auto require(T&& x) -> valid<
        decltype(returns<is_iterator<_>>(adl::adl_begin(x))),
        decltype(returns<is_iterator<_>>(adl::adl_end(x)))
    >;
};
// end-sample

template<class T>
struct is_variant
: std::false_type
{};

template<class T>
struct is_variant<const T>
: is_variant<T>
{};

template<class... Ts>
struct is_variant<boost::variant<Ts...>>
: std::true_type
{};

using tick::trait;

// sample(fit_print)
FIT_STATIC_FUNCTION(print) = fit::fix(fit::conditional(
    [](auto, const std::string& x)
    {
        std::cout << x << std::endl;
    },
    [](auto self, const auto& range, 
        TICK_PARAM_REQUIRES(trait<is_range>(range)))
    {
        for(const auto& x:range) self(x);
    },
    [](auto self, const auto& sequence, 
        TICK_PARAM_REQUIRES(trait<boost::fusion::traits::is_sequence>(sequence)))
    {
        boost::fusion::for_each(sequence, self);
    },
    [](auto self, const auto& variant, 
        TICK_PARAM_REQUIRES(trait<is_variant>(variant)))
    {
        boost::apply_visitor(fit::result<void>(self), variant);
    },
    [](auto, const auto& x, 
        TICK_PARAM_REQUIRES(trait<is_streamable>(std::cout, x)))
    {
        std::cout << x << std::endl;
    }
));
// end-sample

int main()
{
    static_assert(is_range<const std::vector<int>>(), "Failed is_range");

    static_assert(is_range<int[5]>(), "Failed is_range");
    print("hello");
    print("3");
    
    int i = 3;
    print(i);

    std::vector<int> r = { 3, 3, 3 };
    print(r);

    auto t = std::make_tuple(3, 3, 3);
    print(t);

    auto f = boost::fusion::make_vector(3, 3, 3);
    print(f);

    boost::variant<int, std::string> v1 = 3;
    print(v1);
    boost::variant<int, std::string> v2 = "3";
    print(v2);

    std::vector<std::string> rs = { "hello", "world"};
    print(rs);

    boost::variant<int, std::vector<int>> v3 = r;
    print(v3);

    std::vector<boost::variant<int, std::string>> rv = { 3, "3", 3 };
    print(rv);

    auto m = std::make_tuple(3, r, v1, v3, rv);
    print(m);
}
