#include <iostream>
#include <vector>
#include <list>
#include <tick/builder.h>
#include <tick/requires.h>
#include <tick/trait_check.h>
#include <tick/tag.h>
#include <fit/lambda.h>
#include <fit/function.h>
#include <fit/conditional.h>
#include <fit/reveal.h>

// sample(advance_traits)
TICK_TRAIT(is_incrementable)
{
    template<class T>
    auto require(T&& x) -> valid<
        decltype(returns<T>(x++)),
        decltype(returns<std::add_lvalue_reference_t<T>>(++x))
    >;
};
TICK_TRAIT(is_decrementable, is_incrementable<_>)
{
    template<class T>
    auto require(T&& x) -> valid<
        decltype(returns<T>(x--)),
        decltype(returns<std::add_lvalue_reference_t<T>>(--x))
    >;
};
TICK_TRAIT(is_advanceable, is_decrementable<_>)
{
    template<class T, class Number>
    auto require(T&& x, Number n) -> valid<
        decltype(returns<std::add_lvalue_reference_t<T>>(x += n))
    >;
};
// end-sample

// sample(advance_tag)
template<class Iterator>
void advance_impl(Iterator& it, int n, tick::tag<is_advanceable>)
{
    it += n;
}

template<class Iterator>
void advance_impl(Iterator& it, int n, tick::tag<is_decrementable>)
{
    if (n > 0) while (n--) ++it;
    else 
    {
        n *= -1;
        while (n--) --it;
    }
}

template<class Iterator>
void advance_impl(Iterator& it, int n, tick::tag<is_incrementable>)
{
    while (n--) ++it;
}

template<class Iterator, TICK_REQUIRES(is_incrementable<Iterator>())>
void advance(Iterator& it, int n)
{
    advance_impl(it, n, tick::most_refined<is_advanceable<Iterator, int>>());
}
// end-sample

void check_list()
{
    std::list<int> l = { 1, 2, 3, 4, 5, 6 };
    auto iterator = l.begin();
    advance(iterator, 4);
    std::cout << *iterator << std::endl;
}

void check_reverse_list()
{
    std::list<int> l = { 1, 2, 3, 4, 5, 6 };
    auto iterator = l.end();
    advance(iterator, -4);
    std::cout << *iterator << std::endl;
}

void check_vector()
{
    std::vector<int> v = { 1, 2, 3, 4, 5, 6 };
    auto iterator = v.begin();
    advance(iterator, 4);
    std::cout << *iterator << std::endl;
}

struct foo 
{
    foo& operator+=(int)
    {
        return *this;
    }
};

int main()
{
    // TICK_TRAIT_CHECK(is_advanceable<foo>);
    // advance(foo(), 1);
    // fit::reveal(advance)(foo(), 1);
    check_list();
    check_reverse_list();
    check_vector();
}
