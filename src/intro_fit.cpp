
#include <iostream>
#include <vector>
#include <list>
#include <tick/builder.h>
#include <tick/requires.h>
#include <fit/lambda.h>
#include <fit/function.h>
#include <fit/conditional.h>
#include <fit/pipable.h>
#include <fit/reveal.h>

namespace fo {

// sample(fit_initialize_function_object)
struct sum_f
{
    template<class T, class U>
    auto operator()(T x, U y) const
    {
        return x + y;
    }
};

const constexpr sum_f sum = {};
// end-sample

void test_it()
{
    // sample(fit_sum_result)
    auto three = sum(1, 2);
    // end-sample
    std::cout << three << std::endl;
}

}

namespace lambda {

// sample(fit_initialize_lambda)
const constexpr auto sum = FIT_STATIC_LAMBDA(auto x, auto y)
{
    return x + y;
};
// end-sample

void test_it()
{
    auto three = sum(1, 2);
    std::cout << three << std::endl;
}

}

namespace pipable_fo {
using fo::sum_f;
// sample(fit_initialize_pipable_adaptor)
const constexpr fit::pipable_adaptor<sum_f> sum = {};
// end-sample

void test_it()
{
    // sample(fit_pipable_example)
    auto three = 1 | sum(2);
    // end-sample
    std::cout << three << std::endl;
    auto n = sum(1, 2);
}

}

namespace pipable_lambda {

// sample(fit_initialize_pipable_lambda)
const constexpr auto sum = fit::pipable(FIT_STATIC_LAMBDA(auto x, auto y)
{
    return x + y;
});
// end-sample
void test_it()
{
    auto three = 1 | sum(2);
    std::cout << three << std::endl;
    auto n = sum(1, 2);
}

}

namespace pipable_function {

// sample(fit_initialize_pipable_function)
FIT_STATIC_FUNCTION(sum) = fit::pipable([](auto x, auto y)
{
    return x + y;
});
// end-sample
void test_it()
{
    auto three = 1 | sum(2);
    std::cout << three << std::endl;
    auto n = sum(1, 2);
}

}


int main() {

    fo::test_it();
    lambda::test_it();
    pipable_fo::test_it();
    pipable_lambda::test_it();
    pipable_function::test_it();
}

