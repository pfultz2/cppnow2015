
#include <tick/builder.h>
#include <tick/requires.h>

namespace refinements {

// sample(tick_refinements1)
TICK_TRAIT(is_incrementable, std::is_default_constructible<_>)
{
    template<class T>
    auto require(T&& x) -> valid<
        decltype(x++),
        decltype(++x)
    >;
};
// end-sample

// sample(tick_refinements2)
TICK_TRAIT(is_equality_comparable, 
    std::is_default_constructible<_1>, 
    std::is_default_constructible<_2>)
{
    template<class T, class U>
    auto require(T&& x, U&& y) -> valid<
        decltype(x == y),
        decltype(x != y)
    >;
};
// end-sample

void test_it()
{
    static_assert(is_incrementable<int>(), "");
    static_assert(is_equality_comparable<int, int>(), "");
}

}


namespace check_bool {

// sample(tick_check_bool)
TICK_TRAIT(is_equality_comparable)
{
    template<class T, class U>
    auto require(T&& x, U&& y) -> valid<
        decltype(returns<bool>(x == y)),
        decltype(returns<bool>(x != y))
    >;
};
// end-sample

void test_it()
{
    static_assert(is_equality_comparable<int, int>(), "");
}

}

namespace check_integral_constant {
// sample(tick_check_fundamental)
TICK_TRAIT(is_equality_comparable)
{
    template<class T, class U>
    auto require(T&& x, U&& y) -> valid<
        decltype(returns<std::is_fundamental<_>>(x == y)),
        decltype(returns<std::is_fundamental<_>>(x != y))
    >;
};
// end-sample

void test_it()
{
    static_assert(is_equality_comparable<int, int>(), "");
}

}

namespace type_template {
// sample(tick_is_metafunction_class)
TICK_TRAIT(is_metafunction_class)
{
    template<class T>
    auto require(const T& x) -> valid<
        has_type<typename T::type>,
        has_template<T::template apply>
    >;
};
// end-sample

template<class T>
struct always
{
    typedef always type;

    template<class...>
    struct apply
    {
        typedef T type;
    };
};

void test_it()
{
    static_assert(is_metafunction_class<always<void>>(), "");
}

}

namespace traits {

TICK_TRAIT(is_incrementable)
{
    template<class T>
    auto require(T&& x) -> valid<
        decltype(x++),
        decltype(++x)
    >;
};

}

namespace member_requires {
using traits::is_incrementable;
// sample(tick_member_requires)
template<class T>
struct foo
{
    T x;

    TICK_MEMBER_REQUIRES(is_incrementable<T>())
    void up()
    {
        x++;
    }
};
// end-sample
void test_it()
{
    foo<int> x;
    x.up();
}

}

namespace param_requires1 {
using traits::is_incrementable;
// sample(tick_param_requires1)
auto increment = [](auto& x, TICK_PARAM_REQUIRES(is_incrementable<decltype(x)>()))
{
    x++;
};
// end-sample
void test_it()
{
    int i = 1;
    increment(i);
}

}

namespace param_requires2 {
using traits::is_incrementable;
using tick::trait;
// sample(tick_param_requires2)
auto increment = [](auto& x, TICK_PARAM_REQUIRES(trait<is_incrementable>(x)))
{
    x++;
};
// end-sample
void test_it()
{
    int i = 1;
    increment(i);
}

}

int main() {
    refinements::test_it();
    check_bool::test_it();
    check_integral_constant::test_it();
    type_template::test_it();
    member_requires::test_it();
    param_requires1::test_it();
    param_requires2::test_it();

}
