
#include <vector>
#include <iostream>
#include <type_traits>
#include <boost/fusion/container/generation/make_vector.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/adapted/std_tuple.hpp>
#include <boost/fusion/support/is_sequence.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/algorithm/transformation/filter_if.hpp>
#include <tick/builder.h>
#include <tick/builder.h>
#include <tick/requires.h>

using namespace boost::fusion;
using namespace boost::mpl;

template<class F>
struct predicate
{
    template<class T>
    struct apply
    {
        using type = decltype(std::declval<F>()(std::declval<T>()));
    };
};

template<class Tuple, class Predicate>
auto simple_filter(Tuple&& t, Predicate)
{
    return filter_if<predicate<Predicate>>(std::forward<Tuple>(t));
}

template<class T>
using enhance = tick::integral_constant<typename T::value_type, T::value>;

template<class T>
using is_integral = enhance<std::is_integral<T>>;

template<class T>
using is_floating_point = enhance<std::is_floating_point<T>>;

// sample(filter_numbers)
template<class Tuple>
auto filter_numbers(const Tuple& t)
{
    return simple_filter(t, [](auto x) 
    { 
        return is_integral<decltype(x)>() or is_floating_point<decltype(x)>(); 
    });
}
// end-sample

template<class Tuple>
void print_numbers(const Tuple& t)
{
    for_each(t, [](auto x) 
    { 
        std::cout << x << std::endl; 
    });
}

// sample(tick_is_fusable)
TICK_TRAIT(is_fusable, std::is_copy_constructible<_>)
{
    template<class T>
    auto require(T&& x) -> valid<
        decltype(returns<is_sequence<_>>(x.as_fusion_sequence()))
    >;
};
// end-sample


struct fusable 
{
    int x;
    std::tuple<int> as_fusion_sequence() const
    {
        return std::make_tuple(x);
    }
};

int main() 
{
    static_assert(is_fusable<fusable>(), "Not fusable");
    print_numbers(filter_numbers(std::make_tuple(1, 2.5, std::vector<int>())));
}
