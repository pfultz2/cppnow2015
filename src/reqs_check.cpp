
#include <boost/concept_check.hpp>
#include <iostream>
#include <type_traits>


// sample(concept_check_incrementable)
template<class T>
struct Incrementable
{
    BOOST_CONCEPT_USAGE(Incrementable)
    {
        x++;
        ++x;
    }
    T x;
};
// end-sample

// sample(increment_concept_check)
template<class T>
void increment(T& x)
{
    ++x;
}

template<class T>
void twice(T& x)
{
    BOOST_CONCEPT_ASSERT((Incrementable<T>));
    increment(x);
    increment(x);
}
// end-sample

struct foo {};

int main() {
    int i = 1;
    twice(i);
    std::cout << i; // Prints 3

    // foo f;
    // twice(f);
}
