## Modern generic programming using the Tick and Fit libraries

Paul Fultz II

---------

## Motivation

%%unbound_increment%%

---

## Motivation

```cpp
foo f;
twice(f);
```

---

##  Motivation

```
reqs.cpp:9:5: error: cannot increment value of type 'foo'
    ++x;
    ^ ~
reqs.cpp:15:5: note: in instantiation of function template specialization 'increment<foo>' requested here
    increment(x);
    ^
reqs.cpp:28:5: note: in instantiation of function template specialization 'twice<foo>' requested here
    twice(f);
    ^
```

---

## Type requirements

- Specify type requirements(or concepts)
    - Set of valid expressions that can be performed on a type or types <!-- .element: class="fragment" -->
    - Provide documentation on these requirements <!-- .element: class="fragment" -->
    - Check these type requirements with the compiler <!-- .element: class="fragment" -->

---

## Using Boost.ConceptCheck

%%concept_check_incrementable%%

---

## Using Boost.ConceptCheck

%%increment_concept_check%%

---

## Using Boost.ConceptCheck

```
reqs_check.cpp:13:10: error: cannot increment value of type 'foo'
        x++;
        ~^
/usr/local/include/boost/concept/usage.hpp:16:43: note: in instantiation of member function 'Incrementable<foo>::~Incrementable' requested here
    ~usage_requirements() { ((Model*)0)->~Model(); }
                                          ^
/usr/local/include/boost/concept/detail/general.hpp:38:42: note: in instantiation of member function 'boost::concepts::usage_requirements<Incrementable<foo> >::~usage_requirements' requested here
    static void failed() { ((Model*)0)->~Model(); }
                                         ^
reqs_check.cpp:11:5: note: in instantiation of member function 'boost::concepts::requirement<boost::concepts::failed
      ************boost::concepts::usage_requirements<Incrementable<foo> >::************>::failed' requested here
    BOOST_CONCEPT_USAGE(Incrementable)
    ^
/usr/local/include/boost/concept/usage.hpp:29:7: note: expanded from macro 'BOOST_CONCEPT_USAGE'
      BOOST_CONCEPT_ASSERT((boost::concepts::usage_requirements<model>)); \
      ^
/usr/local/include/boost/concept/assert.hpp:43:5: note: expanded from macro 'BOOST_CONCEPT_ASSERT'
    BOOST_CONCEPT_ASSERT_FN(void(*)ModelInParens)
    ^
/usr/local/include/boost/concept/detail/general.hpp:78:51: note: expanded from macro 'BOOST_CONCEPT_ASSERT_FN'
    &::boost::concepts::requirement_<ModelFnPtr>::failed>    \
                                                  ^
reqs_check.cpp:44:5: note: in instantiation of function template specialization 'twice<foo>' requested here
    twice(f);
    ^
reqs_check.cpp:14:9: error: cannot increment value of type 'foo'
        ++x;
        ^ ~
reqs_check.cpp:24:5: error: cannot increment value of type 'foo'
    ++x;
    ^ ~
reqs_check.cpp:31:5: note: in instantiation of function template specialization 'increment<foo>' requested here
    increment(x);
    ^
reqs_check.cpp:44:5: note: in instantiation of function template specialization 'twice<foo>' requested here
    twice(f);
    ^
```

---

## Limitations of Boost.ConceptCheck

- No overloading <!-- .element: class="fragment" -->
- Doesn't reduce the number of errors <!-- .element: class="fragment" -->

---

## Using ConceptsLite

```cpp
template<class T>
concept bool Incrementable()
{
    return requires(T&& x)
    {
        x++;
        ++x;
    };
}
```

---

## Using ConceptsLite

```cpp
template<class T>
void increment(T& x)
{
    ++x;
}

template<class T> requires Incrementable<T>()
void twice(T& x) 
{
    increment(x);
    increment(x);
}
```

---

## Using ConceptsLite

```
req-concepts.cpp:37:12: error: cannot call function ‘void twice(T&) [with T = foo]’
     twice(f);
            ^
req-concepts.cpp:22:6: note:   constraints not satisfied
 void twice(T& x) 
      ^
req-concepts.cpp:22:6: note:   concept ‘Incrementable<foo>()’ was not satisfied
```

---

## Using Tick

- Create type traits to check type requirements <!-- .element: class="fragment" -->
- Naming: <!-- .element: class="fragment" -->
    - Concept: `DefaultConstructible`
    - Type trait: `std::is_default_constructible`

---

## Using Tick

%%tick_incrementable%%

---

## Using Tick

%%tick_increment%%

---

## Using Tick

```
reqs_tick.cpp:43:5: error: no matching function for call to 'twice'
    twice(f);
    ^~~~~
reqs_tick.cpp:27:19: note: candidate template ignored: disabled by 'enable_if' [with T = foo, TickPrivateBool__LINE__ = true]
template<class T, TICK_REQUIRES(is_incrementable<T>())>
                  ^
```

---

## Refinements

%%tick_refinements1%%

---

## Refinements

%%tick_refinements2%%

---

## Check returns

%%tick_check_bool%%

---

## Check returns

%%tick_check_fundamental%%

---

## Checking for nested types and template

%%tick_is_metafunction_class%%

---

## Checking requirements

%%tick_increment%%

---

## Checking requirements

%%tick_member_requires%%

---

## Checking requirements

%%tick_param_requires1%%

---

## Checking requirements

%%tick_param_requires2%%

---

## Concepts Lite vs Tick

- Interoperability with type traits

%%tick_is_fusable%%

---

## Concepts Lite vs Tick

- Specialization: Types opt-in to a concept implicitly, specialization allows types to opt-out explicitly
    - Important with overloading <!-- .element: class="fragment" -->
    - Unconstrained templates <!-- .element: class="fragment" -->

---

## Concepts Lite vs Tick

- Dependent typing

%%filter_numbers%%

---

## Concepts Lite vs Tick

- First class citizen

%%tick_param_requires2%%

---

## Concepts Lite vs Tick

- Overloading
    - Subsuming vs tag dispatching

